package top.cavecraft.arena;

import org.bukkit.*;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import top.cavecraft.arena.challenges.ChickenChallenge;
import top.cavecraft.arena.challenges.ItemChallenge;
import top.cavecraft.arena.challenges.SkeletonChallenge;
import top.cavecraft.arena.challenges.TagChallenge;
import top.cavecraft.cclib.ICCLib;

import java.util.*;

public final class Arena extends JavaPlugin implements Listener {
    public static ICCLib cclib;
    public static Arena plugin;
    boolean challengeActive = false;
    List<UUID> noFall = new ArrayList<>();
    List<UUID> inGame = new ArrayList<>();
    List<Challenge> challenges = new ArrayList<>();
    List<String> records = Arrays.asList(
            "records.chirp",
            "records.cat",
            "records.mall",
            "records.far",
            "records.mellohi",
            "records.stal",
            "records.wait",
            "records.ward",
            "records.strad"
    );

    public static List<Material> items = Arrays.asList(
            Material.DIAMOND_BOOTS,
            Material.DIAMOND_CHESTPLATE,
            Material.DIAMOND_LEGGINGS,
            Material.DIAMOND_HELMET,
            Material.DIAMOND_SWORD,
            Material.DIAMOND_AXE,
            Material.GOLDEN_APPLE,
            Material.IRON_BOOTS,
            Material.IRON_CHESTPLATE,
            Material.IRON_LEGGINGS,
            Material.IRON_HELMET,
            Material.IRON_SWORD,
            Material.IRON_AXE,
            Material.CHAINMAIL_BOOTS,
            Material.CHAINMAIL_CHESTPLATE,
            Material.CHAINMAIL_LEGGINGS,
            Material.CHAINMAIL_HELMET,
            Material.STONE_SWORD,
            Material.STONE_AXE
    );

    private void killFX(Player player, Player damager) {
        getServer().broadcastMessage(ChatColor.GOLD + player.getName() + ChatColor.RED + " was killed by " + ChatColor.GOLD + damager.getName() + ChatColor.RED + "!");
        respawn(player);
        player.playSound(player.getLocation(), Sound.ENDERMAN_DEATH, 1, 0.7f);
        damager.playSound(damager.getLocation(), Sound.ANVIL_LAND, 1, 1.5f);
        damager.getInventory().addItem(new ItemStack(Material.GOLDEN_APPLE));
        cclib.awardGems(damager, ICCLib.IGameUtils.GemAmount.EXTRA_LARGE);
    }

    private void deathFX(Player player) {
        getServer().broadcastMessage(ChatColor.GOLD + player.getName() + ChatColor.RED + " died.");
        respawn(player);
        player.playSound(player.getLocation(), Sound.ENDERMAN_DEATH, 1, 0.7f);
    }

    private void respawn(Player player) {
        player.setExp(0);
        player.getInventory().clear();
        player.getInventory().setArmorContents(new ItemStack[]{
                new ItemStack(Material.LEATHER_BOOTS),
                new ItemStack(Material.LEATHER_LEGGINGS),
                new ItemStack(Material.LEATHER_CHESTPLATE),
                new ItemStack(Material.LEATHER_HELMET)
        });
        player.setHealth(20);
        player.getInventory().addItem(new ItemStack(Material.WOOD_SWORD));
        player.getInventory().addItem(new ItemStack(Material.FISHING_ROD));
        player.getInventory().addItem(new ItemStack(Material.COMPASS));
        player.setFireTicks(0);
        player.teleport(new Location(Bukkit.getWorld("world"), 10, 121, 0, 90, 0));
        player.setGameMode(GameMode.SURVIVAL);
        inGame.remove(player.getUniqueId());
    }

    @Override
    public void onEnable() {
        plugin = this;
        getServer().getPluginManager().registerEvents(this, this);
        cclib = (ICCLib) getServer().getPluginManager().getPlugin("CCLib");

        cclib.getScoreboardManager().setTitleOfAll(ChatColor.GOLD + "ARENA");
        cclib.getScoreboardManager().addBlankLineToAll();
        cclib.getScoreboardManager().addLineToAll("desc", "Kill players to");
        cclib.getScoreboardManager().addLineToAll("desc3", "get golden apples.");
        cclib.getScoreboardManager().addBlankLineToAll();
        cclib.getScoreboardManager().addLineToAll("wmak", ChatColor.GOLD + "-  cavecraft.top  -");

        challenges.add(new ItemChallenge());
        challenges.add(new ChickenChallenge());
        challenges.add(new SkeletonChallenge());
        challenges.add(new TagChallenge());
        Collections.shuffle(challenges);

        getServer().getScheduler().runTaskTimer(this, () -> {
            if (getServer().getOnlinePlayers().size() >= 1 && getServer().getOnlinePlayers().size() < 3) {
                Location highest = new Location(Bukkit.getWorld("world"), (int)Math.round((Math.random() * 85) - 42.5), 64, (int)Math.round(Math.random() * 85) - 42.5);
                while (highest.getBlock().getType() != Material.AIR) {
                    highest.setY(highest.getBlockY() + 1);
                }
                Zombie zombie = ((Zombie)highest.getWorld().spawnEntity(highest, EntityType.ZOMBIE));
                zombie.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 999999, 2, true));
                EntityEquipment equipment = zombie.getEquipment();
                equipment.setHelmet(new ItemStack(Material.CHAINMAIL_HELMET));
                equipment.setChestplate(new ItemStack(Material.CHAINMAIL_CHESTPLATE));
                equipment.setBoots(new ItemStack(Material.CHAINMAIL_BOOTS));
                equipment.setLeggings(new ItemStack(Material.CHAINMAIL_LEGGINGS));
                equipment.setItemInHand(new ItemStack(Material.STONE_SWORD));
            }

            if (getServer().getOnlinePlayers().size() > 0) {
                for (int i = 0; i < Math.random() * 8; i++) {
                    Location highest = new Location(Bukkit.getWorld("world"), (int) Math.round((Math.random() * 200) - 100), 64, (int) Math.round(Math.random() * 200) - 100);
                    while (highest.getBlock().getType() != Material.AIR) {
                        highest.setY(highest.getBlockY() + 1);
                    }
                    Collections.shuffle(items);
                    highest.getWorld().dropItem(highest, new ItemStack(items.get(0)));
                }
            }

            Collection<? extends Player> players = getServer().getOnlinePlayers();
            for (Player player : players) {
                if (player.getLocation().getY() < 119) {
                    cclib.awardGems(player, ICCLib.IGameUtils.GemAmount.SMALL);
                }
            }
        }, 600, 600);

        getServer().getScheduler().runTaskTimer(this, () -> {
            Challenge oldPick = challenges.get(challenges.size() - 1);
            challenges.remove(oldPick);
            challenges.add(0, oldPick);
            int delay = 0;
            for (Challenge challenge : challenges) {
                int finalDelay = delay;
                getServer().getScheduler().runTaskLater(this, () -> {
                    Collection<? extends Player> players = getServer().getOnlinePlayers();
                    for (Player player : players) {
                        player.resetTitle();
                        player.sendTitle(ChatColor.GOLD + challenge.getName(), "");
                        player.playSound(player.getLocation(), Sound.NOTE_STICKS, 1, (float) (((float) finalDelay / 4.5) % 2.0));
                    }
                }, delay);
                delay += 3;
            }

            getServer().getScheduler().runTaskLater(this, () -> {
                challenges.get(challenges.size() - 1).startChallenge();
                Collection<? extends Player> players = getServer().getOnlinePlayers();
                for (Player player : players) {
                    player.sendTitle(ChatColor.GOLD + challenges.get(challenges.size() - 1).getName(), ChatColor.RED + "CHALLENGE CHOSEN!");
                    player.playSound(player.getLocation(), Sound.VILLAGER_YES, 2, 1);
                }
                challengeActive = true;
            }, delay);

            getServer().getScheduler().runTaskLater(this, () -> {
                challenges.get(challenges.size() - 1).endChallenge();
                challengeActive = false;
            }, delay + 1600);
        }, 1000, 2600);

        getServer().getScheduler().runTaskTimer(this, () -> {
            Collections.shuffle(records);
            Collection<? extends Player> players = getServer().getOnlinePlayers();
            for (Player player : players) {
                player.playSound(new Location(Bukkit.getWorld("world"), 0, 121, 0), records.get(0), 2.5f, 1);
            }
        }, 400, 4000);

        getServer().getScheduler().runTaskTimer(this, () -> {
            Collection<? extends Player> players = getServer().getOnlinePlayers();
            if (players.size() > 1) {
                for (Player player : players) {
                    Player target = players.stream().min(Comparator.comparing((Player other) -> (player == other ? 99999 : (player.getLocation().getY() * 100) + player.getLocation().distanceSquared(other.getLocation())))).get();
                    player.setCompassTarget(target.getLocation());
                    cclib.createActionBarMessage(ChatColor.RED + "Compass Target: " + ChatColor.GOLD + target.getName()).sendTo(player);
                }
            }
        }, 6, 6);
    }

    @EventHandler
    public void join(PlayerJoinEvent event) {
        respawn(event.getPlayer());
    }

    @EventHandler
    public void kill(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
            Player player = (Player)event.getEntity();
            Player damager = (Player)event.getDamager();
            if (event.getFinalDamage() >= player.getHealth()) {
                killFX(player, damager);
                event.setCancelled(true);
            }
        } else if (event.getDamager() instanceof Player && event.getEntity() instanceof Zombie) {
            Player damager = (Player)event.getDamager();
            if (event.getFinalDamage() >= ((Zombie) event.getEntity()).getHealth()) {
                damager.getInventory().addItem(new ItemStack(Material.GOLDEN_APPLE));
                event.setCancelled(true);
                event.getEntity().remove();
                cclib.awardGems(damager, ICCLib.IGameUtils.GemAmount.MEDIUM);
            }
        } else if (event.getEntity() instanceof Player) {
            Player player = (Player)event.getEntity();
            if (event.getFinalDamage() >= player.getHealth()) {
                deathFX(player);
                event.setCancelled(true);
            }
        }

        if (challengeActive) {
            challenges.get(challenges.size() - 1).attackEvt(event);
        }
    }

    @EventHandler
    public void hunger(FoodLevelChangeEvent event) {
        event.setFoodLevel(20);
    }

    @EventHandler
    public void damage(EntityDamageEvent event) {
        if (event.getCause() == EntityDamageEvent.DamageCause.FALL && noFall.contains(event.getEntity().getUniqueId())) {
            noFall.remove(event.getEntity().getUniqueId());
            inGame.add(event.getEntity().getUniqueId());
            event.setCancelled(true);
        } else if (event.getEntity().getLocation().getY() >= 119) {
            event.setCancelled(true);
        } else if (event.getEntity() instanceof Player && event.getCause() != EntityDamageEvent.DamageCause.ENTITY_ATTACK && event.getDamage() >= ((Player) event.getEntity()).getHealth()) {
            deathFX((Player)event.getEntity());
            event.setCancelled(true);
        }
    }

    
    @EventHandler
    public void breakEvt(BlockBreakEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void pickupItem(PlayerPickupItemEvent event) {
        if (!challengeActive) {
            event.setCancelled(true);
        } else {
            challenges.get(challenges.size() - 1).pickupEvt(event);
        }
        if (items.contains(event.getItem().getItemStack().getType())) {
            event.setCancelled(false);
        } else if(!challengeActive) {
            event.getItem().remove();
        }
    }

    @EventHandler
    public void blockDespawn(BlockPhysicsEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void blockDespawn(BlockFadeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void blockDespawn(BlockDamageEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void blockDespawn(BlockFormEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void blockDespawn(BlockGrowEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void water(BlockFromToEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void fire(BlockIgniteEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void move(PlayerMoveEvent event) {
        if (!inGame.contains(event.getPlayer().getUniqueId()) && !noFall.contains(event.getPlayer().getUniqueId()) && event.getTo().distance(new Location(Bukkit.getWorld("world"), 0, 121, 0)) >= 21) {
            if (Math.abs(event.getTo().getX()) > 5 && Math.abs(event.getTo().getZ()) > 5) {
                ((CraftPlayer)event.getPlayer()).setVelocity(event.getPlayer().getVelocity().setY(0).normalize().multiply(Math.random() * 40).setY(1));
            }
            event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.BAT_TAKEOFF, 1, 0.7f);
            noFall.add(event.getPlayer().getUniqueId());
        }
    }

    @EventHandler
    public void drop(PlayerDropItemEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void interact(PlayerInteractEvent event) {
        if (challengeActive) {
            challenges.get(challenges.size() - 1).interactEvt(event);
        }
    }
}
