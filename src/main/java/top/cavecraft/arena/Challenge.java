package top.cavecraft.arena;

import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public interface Challenge {
    void startChallenge();
    void interactEvt(PlayerInteractEvent event);
    void pickupEvt(PlayerPickupItemEvent event);
    void attackEvt(EntityDamageByEntityEvent event);
    void endChallenge();
    String getName();
}
