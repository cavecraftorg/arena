package top.cavecraft.arena.challenges;

import org.bukkit.*;
import org.bukkit.entity.*;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import top.cavecraft.arena.Challenge;
import top.cavecraft.cclib.ICCLib;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.bukkit.Bukkit.getServer;
import static top.cavecraft.arena.Arena.*;

public class ChickenChallenge implements Challenge {
    Map<UUID, Integer> scores = new HashMap<>();

    @Override
    public void startChallenge() {
        plugin.getServer().broadcastMessage(ChatColor.GOLD + "CHALLENGE: Kill chickens and leave pigs alone!");
        for (int i = 0; i < 100; i++) {
            Location highest = new Location(Bukkit.getWorld("world"), (int)Math.round((Math.random() * 150) - 75), 64, (int)Math.round(Math.random() * 150) - 75);
            while (highest.getBlock().getType() != Material.AIR) {
                highest.setY(highest.getBlockY() + 1);
            }
            highest.getWorld().spawnEntity(highest, EntityType.PIG);
        }
        for (int i = 0; i < 200; i++) {
            Location highest = new Location(Bukkit.getWorld("world"), (int)Math.round((Math.random() * 150) - 75), 64, (int)Math.round(Math.random() * 150) - 75);
            while (highest.getBlock().getType() != Material.AIR) {
                highest.setY(highest.getBlockY() + 1);
            }
            highest.getWorld().spawnEntity(highest, EntityType.CHICKEN);
        }
    }

    @Override
    public void interactEvt(PlayerInteractEvent event) {

    }

    @Override
    public void pickupEvt(PlayerPickupItemEvent event) {
        event.setCancelled(true);
    }

    @Override
    public void attackEvt(EntityDamageByEntityEvent event) {
        if (event.getEntity().getType() == EntityType.PIG) {
            scores.put(event.getDamager().getUniqueId(), scores.getOrDefault(event.getDamager().getUniqueId(), 0) - 2);
            event.getDamager().sendMessage(ChatColor.GOLD + "Score: " + scores.get(event.getDamager().getUniqueId()));
            event.getEntity().remove();
            event.setCancelled(true);
            ((Player)event.getDamager()).playSound(event.getEntity().getLocation(), Sound.PIG_DEATH, 1, 1);
        } else if (event.getEntity().getType() == EntityType.CHICKEN) {
            scores.put(event.getDamager().getUniqueId(), scores.getOrDefault(event.getDamager().getUniqueId(), 0) + 2);
            event.getDamager().sendMessage(ChatColor.GOLD + "Score: " + scores.get(event.getDamager().getUniqueId()));
            event.getEntity().remove();
            event.setCancelled(true);
            ((Player)event.getDamager()).playSound(event.getEntity().getLocation(), Sound.CHICKEN_HURT, 1, 1);
        }
        for (Item item : Bukkit.getWorld("world").getEntitiesByClass(Item.class)) {
            item.remove();
        }
    }

    @Override
    public void endChallenge() {
        for (Chicken chicken : Bukkit.getWorld("world").getEntitiesByClass(Chicken.class)) {
            chicken.remove();
        }
        for (Pig pig : Bukkit.getWorld("world").getEntitiesByClass(Pig.class)) {
            pig.remove();
        }
        for (Item item : Bukkit.getWorld("world").getEntitiesByClass(Item.class)) {
            if (!items.contains(item.getItemStack().getType())) {
                item.remove();
            }
        }
        UUID bestuuid = null;
        int bestscore = -99999;
        Collection<? extends Player> players = getServer().getOnlinePlayers();
        for (Player player : players) {
            if (scores.getOrDefault(player.getUniqueId(), 0) >= bestscore) {
                bestscore = scores.getOrDefault(player.getUniqueId(), 0);
                bestuuid = player.getUniqueId();
            }
        }
        plugin.getServer().broadcastMessage(ChatColor.GREEN + Bukkit.getPlayer(bestuuid).getName() + " won the challenge!");
        cclib.awardGems(Bukkit.getPlayer(bestuuid), ICCLib.IGameUtils.GemAmount.EXTRA_LARGE);
        cclib.awardGems(Bukkit.getPlayer(bestuuid), ICCLib.IGameUtils.GemAmount.EXTRA_LARGE);
        cclib.awardGems(Bukkit.getPlayer(bestuuid), ICCLib.IGameUtils.GemAmount.EXTRA_LARGE);
        cclib.awardGems(Bukkit.getPlayer(bestuuid), ICCLib.IGameUtils.GemAmount.EXTRA_LARGE);
        scores.clear();
    }

    @Override
    public String getName() {
        return "Chicken Farming";
    }
}
