package top.cavecraft.arena.challenges;

import org.bukkit.*;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import top.cavecraft.arena.Challenge;
import top.cavecraft.cclib.ICCLib;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.bukkit.Bukkit.getServer;
import static top.cavecraft.arena.Arena.*;

public class ItemChallenge implements Challenge {
    Map<UUID, Integer> scores = new HashMap<>();

    @Override
    public void startChallenge() {
        plugin.getServer().broadcastMessage(ChatColor.GOLD + "CHALLENGE: Pick up diamonds, and avoid TNT. Player with the most items wins!");
        for (int i = 0; i < 200; i++) {
            Location highest = new Location(Bukkit.getWorld("world"), (int)Math.round((Math.random() * 150) - 75), 64, (int)Math.round(Math.random() * 150) - 75);
            while (highest.getBlock().getType() != Material.AIR) {
                highest.setY(highest.getBlockY() + 1);
            }
            highest.getWorld().dropItem(highest, new ItemStack(Material.DIAMOND));
        }
        for (int i = 0; i < 100; i++) {
            Location highest = new Location(Bukkit.getWorld("world"), (int)Math.round((Math.random() * 150) - 75), 64, (int)Math.round(Math.random() * 150) - 75);
            while (highest.getBlock().getType() != Material.AIR) {
                highest.setY(highest.getBlockY() + 1);
            }
            highest.getWorld().dropItem(highest, new ItemStack(Material.TNT));
        }
    }

    @Override
    public void interactEvt(PlayerInteractEvent event) {

    }

    @Override
    public void pickupEvt(PlayerPickupItemEvent event) {
        if (event.getItem().getItemStack().getType() == Material.TNT) {
            scores.put(event.getPlayer().getUniqueId(), scores.getOrDefault(event.getPlayer().getUniqueId(), 0) - 2);
            event.getPlayer().sendMessage(ChatColor.GOLD + "Score: " + scores.get(event.getPlayer().getUniqueId()));
            event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ITEM_PICKUP, 1, 1);
        } else if (event.getItem().getItemStack().getType() == Material.DIAMOND) {
            scores.put(event.getPlayer().getUniqueId(), scores.getOrDefault(event.getPlayer().getUniqueId(), 0) + 2);
            event.getPlayer().sendMessage(ChatColor.GOLD + "Score: " + scores.get(event.getPlayer().getUniqueId()));
            event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ITEM_PICKUP, 1, 1);
        }
        event.getItem().remove();
        event.setCancelled(true);
    }

    @Override
    public void attackEvt(EntityDamageByEntityEvent event) {

    }

    @Override
    public void endChallenge() {
        for (Item item : Bukkit.getWorld("world").getEntitiesByClass(Item.class)) {
            if (!items.contains(item.getItemStack().getType())) {
                item.remove();
            }
        }
        UUID bestuuid = null;
        int bestscore = -99999;
        Collection<? extends Player> players = getServer().getOnlinePlayers();
        for (Player player : players) {
            if (scores.getOrDefault(player.getUniqueId(), 0) >= bestscore) {
                bestscore = scores.getOrDefault(player.getUniqueId(), 0);
                bestuuid = player.getUniqueId();
            }
        }
        plugin.getServer().broadcastMessage(ChatColor.GREEN + Bukkit.getPlayer(bestuuid).getName() + " won the challenge!");
        cclib.awardGems(Bukkit.getPlayer(bestuuid), ICCLib.IGameUtils.GemAmount.EXTRA_LARGE);
        cclib.awardGems(Bukkit.getPlayer(bestuuid), ICCLib.IGameUtils.GemAmount.EXTRA_LARGE);
        cclib.awardGems(Bukkit.getPlayer(bestuuid), ICCLib.IGameUtils.GemAmount.EXTRA_LARGE);
        cclib.awardGems(Bukkit.getPlayer(bestuuid), ICCLib.IGameUtils.GemAmount.EXTRA_LARGE);
        scores.clear();
    }

    @Override
    public String getName() {
        return "Diamond Hunt";
    }
}
