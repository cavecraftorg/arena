package top.cavecraft.arena.challenges;

import org.bukkit.*;
import org.bukkit.entity.*;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import top.cavecraft.arena.Challenge;
import top.cavecraft.cclib.ICCLib;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.bukkit.Bukkit.getServer;
import static top.cavecraft.arena.Arena.*;

public class SkeletonChallenge implements Challenge {
    Map<UUID, Integer> scores = new HashMap<>();

    @Override
    public void startChallenge() {
        plugin.getServer().broadcastMessage(ChatColor.GOLD + "CHALLENGE: Kill the most skeletons!");
        for (int i = 0; i < 300; i++) {
            Location highest = new Location(Bukkit.getWorld("world"), (int)Math.round((Math.random() * 150) - 75), 64, (int)Math.round(Math.random() * 150) - 75);
            while (highest.getBlock().getType() != Material.AIR) {
                highest.setY(highest.getBlockY() + 1);
            }
            Skeleton skeleton = (Skeleton)highest.getWorld().spawnEntity(highest, EntityType.SKELETON);
            skeleton.getEquipment().setHelmet(new ItemStack(Material.LEATHER_HELMET));
        }
    }

    @Override
    public void interactEvt(PlayerInteractEvent event) {

    }

    @Override
    public void pickupEvt(PlayerPickupItemEvent event) {
        event.setCancelled(true);
    }

    @Override
    public void attackEvt(EntityDamageByEntityEvent event) {
        if (event.getEntity().getType() == EntityType.SKELETON && event.getDamage() >= ((LivingEntity)event.getEntity()).getHealth()) {
            scores.put(event.getDamager().getUniqueId(), scores.getOrDefault(event.getDamager().getUniqueId(), 0) + 2);
            event.getDamager().sendMessage(ChatColor.GOLD + "Score: " + scores.get(event.getDamager().getUniqueId()));
            event.getEntity().remove();
            event.setCancelled(true);
            ((Player)event.getDamager()).playSound(event.getEntity().getLocation(), Sound.SKELETON_DEATH, 1, 1);
        }
    }

    @Override
    public void endChallenge() {
        for (Skeleton skeleton : Bukkit.getWorld("world").getEntitiesByClass(Skeleton.class)) {
            skeleton.remove();
        }
        for (Item item : Bukkit.getWorld("world").getEntitiesByClass(Item.class)) {
            if (!items.contains(item.getItemStack().getType())) {
                item.remove();
            }
        }
        for (Arrow arrow : Bukkit.getWorld("world").getEntitiesByClass(Arrow.class)) {
            arrow.remove();
        }
        UUID bestuuid = null;
        int bestscore = -99999;
        Collection<? extends Player> players = getServer().getOnlinePlayers();
        for (Player player : players) {
            if (scores.getOrDefault(player.getUniqueId(), 0) >= bestscore) {
                bestscore = scores.getOrDefault(player.getUniqueId(), 0);
                bestuuid = player.getUniqueId();
            }
        }
        plugin.getServer().broadcastMessage(ChatColor.GREEN + Bukkit.getPlayer(bestuuid).getName() + " won the challenge!");
        cclib.awardGems(Bukkit.getPlayer(bestuuid), ICCLib.IGameUtils.GemAmount.EXTRA_LARGE);
        cclib.awardGems(Bukkit.getPlayer(bestuuid), ICCLib.IGameUtils.GemAmount.EXTRA_LARGE);
        cclib.awardGems(Bukkit.getPlayer(bestuuid), ICCLib.IGameUtils.GemAmount.EXTRA_LARGE);
        cclib.awardGems(Bukkit.getPlayer(bestuuid), ICCLib.IGameUtils.GemAmount.EXTRA_LARGE);
        scores.clear();
    }

    @Override
    public String getName() {
        return "Archer Horde";
    }
}
