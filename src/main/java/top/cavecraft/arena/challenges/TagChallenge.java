package top.cavecraft.arena.challenges;

import org.bukkit.*;
import org.bukkit.entity.*;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import top.cavecraft.arena.Challenge;
import top.cavecraft.cclib.ICCLib;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.bukkit.Bukkit.getServer;
import static top.cavecraft.arena.Arena.cclib;
import static top.cavecraft.arena.Arena.plugin;

public class TagChallenge implements Challenge {
    UUID tagged;

    @Override
    public void startChallenge() {
        plugin.getServer().broadcastMessage(ChatColor.GOLD + "CHALLENGE: Reverse tag, be \"it\" to win!");
        tagged = Bukkit.getOnlinePlayers().stream().findAny().get().getUniqueId();
        getServer().broadcastMessage(ChatColor.RED + "Go tag " + ChatColor.GOLD + Bukkit.getPlayer(tagged).getName() + ChatColor.RED + ", they're it!");
    }

    @Override
    public void interactEvt(PlayerInteractEvent event) {

    }

    @Override
    public void pickupEvt(PlayerPickupItemEvent event) {
        event.setCancelled(true);
    }

    @Override
    public void attackEvt(EntityDamageByEntityEvent event) {
        if (event.getEntity().getUniqueId() == tagged &&event.getDamager() instanceof Player) {
            getServer().broadcastMessage(ChatColor.RED + "Go tag " + ChatColor.GOLD + event.getDamager().getName() + ChatColor.RED + ", they're it!");
            tagged = event.getDamager().getUniqueId();
            ((Player)event.getDamager()).playSound(event.getEntity().getLocation(), Sound.LEVEL_UP, 1, 2);
        }
    }

    @Override
    public void endChallenge() {
        plugin.getServer().broadcastMessage(ChatColor.GREEN + Bukkit.getPlayer(tagged).getName() + " won the challenge!");
        cclib.awardGems(Bukkit.getPlayer(tagged), ICCLib.IGameUtils.GemAmount.EXTRA_LARGE);
        cclib.awardGems(Bukkit.getPlayer(tagged), ICCLib.IGameUtils.GemAmount.EXTRA_LARGE);
        cclib.awardGems(Bukkit.getPlayer(tagged), ICCLib.IGameUtils.GemAmount.EXTRA_LARGE);
        cclib.awardGems(Bukkit.getPlayer(tagged), ICCLib.IGameUtils.GemAmount.EXTRA_LARGE);
    }

    @Override
    public String getName() {
        return "Anti-Tag";
    }
}
